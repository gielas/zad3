package db;

import java.util.List;

import domain.User;

public interface UserRepository extends Repository<User> {
	
	public List<User> withUsername(String username, PagingInfo page);

}

