package db;

public interface RepositoryCatalog {
	
	public PersonRepository people();
	public AddressRepository address();
	public PermissionRepository permission();
	public UserRepository user();
	
}
