package db.catalogs;

import java.sql.Connection;

import db.AddressRepository;
import db.PermissionRepository;
import db.PersonRepository;
import db.RepositoryCatalog;
import db.UserRepository;
import db.repos.HsqlAddressRepository;
import db.repos.HsqlPermissionRepository;
import db.repos.HsqlPersonRepository;
import db.repos.HsqlUserRepository;

public class HsqlRepositoryCatalog implements RepositoryCatalog{

	Connection connection;
	
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public PersonRepository people() {
		return new HsqlPersonRepository(connection);
	}

	public AddressRepository address() {
		return new HsqlAddressRepository(connection);
	}

	public PermissionRepository permission() {
		return new HsqlPermissionRepository(connection);
	}

	public UserRepository user() {
		return new HsqlUserRepository(connection);
	}

}