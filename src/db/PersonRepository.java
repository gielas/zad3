package db;

import java.util.List;

import domain.Person;

public interface PersonRepository extends Repository<Person> {
	
	public List<Person> withName(String name, PagingInfo page);
	public List<Person> withSurname(String surname, PagingInfo page);
	
}
