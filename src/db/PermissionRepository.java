package db;

import java.util.List;

import domain.Permission;

public interface PermissionRepository extends Repository<Permission> {
	
	public List<Permission> withName(String name, PagingInfo page);
}

