package domain;
import java.util.List;
import java.util.ArrayList;

public class UserPerson {
	private int userId;
	private int personId;
	private List<User> user;
	private List<Person> person;
		
	public UserPerson()
	{
		user = new ArrayList<User>();
		person = new ArrayList<Person>();
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

	public List<Person> getPerson() {
		return person;
	}

	public void setPerson(List<Person> person) {
		this.person = person;
	}
}
