package domain;

import java.util.List;
import java.util.ArrayList;

public class RolePermission {
	private int roleId;
	private int permissionId;
	private List<Permission> permission;
	private List<Role> role;
	
	public RolePermission()
	{
		permission = new ArrayList<Permission>();
		role = new ArrayList<Role>();
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public List<Permission> getPermission() {
		return permission;
	}

	public void setPermission(List<Permission> permission) {
		this.permission = permission;
	}

	public List<Role> getRole() {
		return role;
	}

	public void setRole(List<Role> role) {
		this.role = role;
	}
	
	
}
