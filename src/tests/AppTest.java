package tests;

import java.util.List;
import domain.*;
import db.manager.*;


public class AppTest {

	public static void main(String[] args) {
		
		PersonDbManager person = new PersonDbManager();
        AddressDbManager address = new AddressDbManager();
        PermissionDbManager permission = new PermissionDbManager();
        RoleDbManager role = new RoleDbManager();
        UserDbManager user = new UserDbManager();
        
        /* Wy�wietl wszystkich */
        List<Person> allPersons = person.getAll();
        for(Person p : allPersons){
        	System.out.println(p.getId()+" "+p.getName()+" "+p.getSurname());
        }
        
        /* Usu� id=3 */
        person.deleteById(3);
        
        /* Update */
        Person toUpdate = new Person();
        toUpdate.setName("Damian");
        toUpdate.setSurname("Gie�a�yn");
        toUpdate.setId(1);
        person.update(toUpdate);
    
        /* Wy�wietl wszystkich */
        List<Address> allAddresses = address.getAll();
        for(Address p : allAddresses){
        	System.out.println(p.getId()+" "+p.getCity()+" "+p.getCountry()+" "+p.getStreet());
        }
        
        /* Usu� id=3 */
        address.deleteById(3);
        
        /* Update */
        Address Update = new Address();
        Update.setCity("Gdansk");
        Update.setCountry("Poland");
        Update.setStreet("Brzegi 55");
        Update.setId(2); 
        address.update(Update);
        
        /* Wy�wietl wszystkich */
        List<Role> allRoles = role.getAll();
        for(Role r : allRoles){
        	System.out.println(r.getId()+" "+r.getName());
        }
        
        /* Usu� id=3 */
        role.deleteById(3);
        
        /* Update */
        Role Updates = new Role();
        Updates.setId(2);
        Updates.setName("Admin");
        role.update(Updates);
	}
}
